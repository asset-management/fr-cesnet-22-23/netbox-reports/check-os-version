import json
from extras.scripts import *
from dcim.models import Device
from extras.reports import Report
from django.template import Template, Context
from django.core.mail import send_mail

# Attribute by which the device selection is filtered.
TAG_NAME = "OS_VERSION_CHECK"

# Need for sending the Email about the generated report
SEND_EMAIL = False

# Parametres for Email, if the variable SEND_EMAIL is True
EMAIL = {
    'subject': 'NetBox: Check OS version Report',
    'from_email': 'netbox@example.com',
    'recipient_list': [
       "user@domain.com"
    ],
}

# Template for Email written in Jinja2
EMAIL_FORMAT = """
{% for keys, values in data.items %}
{{ keys|title }}:
            INFO({{ values.info|safe }})
                    {% spaceless %}
                {% for name in values.log %}
                    {% spaceless %}
                        {% if name.1 == 'info' %}
                            {{ name.2|safe }}  -  {{ name.3|safe }}
                        {% endif %}
                    {% endspaceless %}
                {% endfor %}
                    {% endspaceless %}
            SUCCESS({{ values.success|safe }})
                    {% spaceless %}
                {% for name in values.log %}
                    {% spaceless %}
                        {% if name.1 == 'success' %}
                            {{ name.2|safe }}  -  {{ name.3|safe }}
                        {% endif %}
                    {% endspaceless %}
                {% endfor %}
                    {% endspaceless %}
            FAILURE({{ values.failure|safe }})
                    {% spaceless %}
                {% for name in values.log %}
                    {% spaceless %}
                        {% if name.1 == 'failure' %}
                            {{ name.2|safe }}  -  {{ name.3|safe }}
                        {% endif %}
                    {% endspaceless %}
                {% endfor %}
                    {% endspaceless %}
            WARNING({{ values.warning|safe }})
                {% spaceless %}
            {% for name in values.log %}
                {% spaceless %}
                    {% if name.1 == 'warning' %}
                        {{ name.2|safe }}  -  {{ name.3|safe }}
                    {% endif %}
                {% endspaceless %}
            {% endfor %}
                {% endspaceless %}
{% endfor %}
"""

# Timeout for the report run
JOB_TIMEOUT = 300

class DeviceReport(Report):
    description = "Check OS Version."
    job_timeout = JOB_TIMEOUT

    @property
    def name(self):
        return "Device OS version report."

    def test_check_firmware(self):
        devices = Device.objects.filter(tags__name = TAG_NAME)
        results = {
            "log_success": [],
            "log_warning": [],
            "log_failure": []
        }
        for device in devices:
            device_config_context = device.get_config_context()
            wanted_version = device_config_context.get("wanted_version")
            if wanted_version is None:
                continue

            device_platform = device.platform            
            if device_platform is None:
                results["log_warning"].append((device,f"Device doesn't have a platform."))

            elif device_platform.name != wanted_version:
                results["log_failure"].append((device,f"Device doesn't have the wanted version of the firmware -> {device_platform.name}. (Wanted: {wanted_version})"))
            else:
                results["log_success"].append((
                    device,
                    f"Device has the wanted version of the firmware. -> {device_platform.name}. (Wanted: {wanted_version})"))

        for log_type in ["log_failure", "log_warning", "log_success"]:
            log_func = getattr(self, log_type)
            for device, message in results[log_type]:
                log_func(device, message)

    def post_run(self):
        data = json.dumps(self._results)
        if SEND_EMAIL:
            email_template = Template(EMAIL_FORMAT)
            context = Context(
                {
                    'data': json.loads(data)
                }
            )
            message = email_template.render(context)
            send_mail(
                message=message,
                **EMAIL
            )

