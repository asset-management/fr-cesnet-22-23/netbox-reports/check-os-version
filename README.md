# Check OS version

## Popis
Report je určený pre sledovanie a kontrolu aktuálnosti firmwarov zariadení na základe vyplnenej hodnoty v objekte Config Context.

## Instalace
- **[Verze 3.4.X a níže]** Report je nutno vložit do složky reports, která má nejčastěji tuto cestu netbox/reports/.
- **[Verze 3.5.X a výše]** Report je nutné naimportovat z lokální složky. Cesta v Netboxu je Customization -> Reports -> Add
- Viz https://docs.netbox.dev/en/stable/customization/reports/

## Výstup
![alt text](/images/Check-OS-Version.png)

## Prerekvizity
1. Zařízení musí mít přiřazen příslušný Tag - ve reportu proměnná TAG_NAME - výchozí hodnota je OS_VERSION_CHECK
2. Zařízení musí mít přiřazenu v Netboxu platformu (objekt Platform)
3. Zařízení musí mít vytvořen objekt Config Context s klíčem "wanted_version"
4. Vyplněné globální proměnné v reportu

## Postup fungování reportu
1. Report vyselektuje všechna zařízení, která mají užívatelem nastavenej tag 
2. Report si z kazdeho zařízení v objektu Config Contextu vytáhne klíč "wanted_version" s jeho hodnotou
3. V případě, že se wanted_version nenachází v config contextu, instanci přeskočí - nevytvoří se log o této instanci
4. Report si následně vytáhne z každého zařízení jeho platformu
5. V případě, že nemá platformu, vytvoří se warning log o absenci platformy na zařízení
6. Report dále porovná hodnoty názvu platformy a hodnoty v Config Contextu zařízení
7. Pokud se hodnoty neshodují, vytvoří se failure log
8. Pokud se hodnoty shodují, vytvoří se success log
9. Report na konec vypíše logy v následujícím pořadí: 1. Failure log 2. Warning log 3. Success log

## Proměnné
**Pro správné fungování reportu je nutné upravit globální proměnné uvnitř kódu!!!**

#### TAG_NAME
 - Táto proměnná představuje objekt Tag, podle kterého se má výběr zařízení filtrovat.  
 - Výchozí hodnota je nastavena na OS_VERSION_CHECK.
```python
TAG_NAME = "OS_VERSION_CHECK"
```
#### JOB_TIMEOUT
- Táto proměnna představuje timeout pro běh reportu.
- Výchozí hodnota je nastavena na 300 vteřin.
```python
JOB_TIMEOUT = 300
```
#### SEND_EMAIL
 - Tato proměnná představuje možnost posílání emailu o stavu reportu po jeho dokončení. 
 - Výchozí hodnota je nastavena na False.
 ```python
SEND_EMAIL = False
 ```
#### EMAIL_FORMAT
 - Tato proměnná představuje formátování pro zaslaný email.
 - Pro zpracování je použita šablona se syntaxi Jinja v Django provedení.
 - Výsledek reportů je do šablony vložen přes proměnnou s názvem data.
 ```python
EMAIL_FORMAT = """

""" 
```
#### EMAIL
 - Táto proměnná představuje dictionary s parametry pro správné odesílání emailů. 
 - V kódu reportu se skládá z následujících parametrů:
    - **subject** - Předmět emailu
    - **from_email** - Odesílatel emailu
    - **recipient_list** - list příjemců mailu
    - Další parametry se nacházejí v konfiguraci NetBoxu v souboru configuration.py 
        - Dokumentace: **https://docs.djangoproject.com/en/4.2/topics/email/**
```python
EMAIL = {
    'subject': 'NetBox: Check OS version Report',
    'from_email': 'netbox@example.com',
    'recipient_list': [
       "user@domain.com"
    ],
}
```
_**Vzorový email**_
```python
Test_Check_Firmware:
            INFO(0)
                   
            SUCCESS(1)
                    gotex-345  -  /dcim/devices/1/
            FAILURE(2)
                    gotex-344  -  /dcim/devices/2/
               
                    test-device  -  /dcim/devices/9/
            WARNING(1)
                asset-test  -  /dcim/devices/10/

```
**Pro správné fungování je nutně vyplnění atributů v .env souboru "netbox.env"**
```python
EMAIL_FROM=netbox@domain.com
EMAIL_PASSWORD=
EMAIL_PORT=25
EMAIL_SERVER=relay.domain.com
EMAIL_SSL_CERTFILE=
EMAIL_SSL_KEYFILE=
EMAIL_TIMEOUT=5
EMAIL_USERNAME=
```

